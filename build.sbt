name := "ServerFriend"

version := "1.0"

scalaVersion := "2.11.6"

resolvers ++= Seq(
  "spigot-repo" at "https://hub.spigotmc.org/nexus/content/groups/public/"
)

libraryDependencies ++= Seq(
  "org.scala-lang" % "scala-library" % "2.11.6",

  "org.bukkit" % "bukkit" % "1.8.3-R0.1-SNAPSHOT",

  "org.slf4j" % "slf4j-nop" % "1.6.4",

//  ormにslick, コネクションプーリングにbonecpを使う
  "com.typesafe.slick" %% "slick" % "2.1.0",
  "com.jolbox" % "bonecp" % "0.8.0.RELEASE",

//  各種jdbc
  "mysql" % "mysql-connector-java" % "5.1.34",
  "org.xerial" % "sqlite-jdbc" % "3.8.7",
  "org.mariadb.jdbc" % "mariadb-java-client" % "1.1.8",
  "postgresql" % "postgresql" % "9.1-901-1.jdbc4"
)