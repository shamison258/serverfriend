package com.shamison.sql

import com.shamison.sql.sqlite.SqLite

/**
 * Created by shamison on 15/03/09.
 */
object DBmanegar {
  var dbname: String = null
  var sqlite: SqLite = null
  val user = "user"
  val pass = "pass"

  // dbを選択
  def init(dbname: String): Unit = {
    this.dbname = dbname
    dbname match {
      case "sqlite" => {
        sqlite = new SqLite("DBPATH")
      }
    }
  }

  def post(): Unit = {
    dbname match {
      case "sqlite" => {
        sqlite.post()
      }
    }
  }

  def get(): Unit = {
    dbname match {
      case "sqlite" => {
        sqlite.show()
      }
    }
  }
}
