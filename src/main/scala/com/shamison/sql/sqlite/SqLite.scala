package com.shamison.sql.sqlite

import com.jolbox.bonecp.BoneCPDataSource
import scala.slick.driver.SQLiteDriver.simple._

/**
 * Created by shamison on 15/03/09.
 */
//, user: String, pass: String
class SqLite(DBpath: String) {

  val ds = new BoneCPDataSource();
  DSset(DBpath ,"org.sqlite.JDBC")
  val db = Database.forDataSource(ds)

  def init(): Unit ={
    db withSession {
      implicit session =>
        (Tables.suppliers.ddl ++ Tables.coffees.ddl).create
    }
    // Create the tables, including primary and foreign keys
  }

  def post(): Unit = {
    db withSession {
      implicit session =>
        // Insert some suppliers
        Tables.suppliers += (101, "Acme, Inc.",      "99 Market Street", "Groundsville", "CA", "95199")
        Tables.suppliers += ( 49, "Superior Coffee", "1 Party Place",    "Mendocino",    "CA", "95460")
        Tables.suppliers += (150, "The High Ground", "100 Coffee Lane",  "Meadows",      "CA", "93966")

        // Insert some coffees (using JDBC's batch insert feature, if supported by the DB)
        Tables.coffees ++= Seq(
          ("Colombian",         101, 7.99, 0, 0),
          ("French_Roast",       49, 8.99, 0, 0),
          ("Espresso",          150, 9.99, 0, 0),
          ("Colombian_Decaf",   101, 8.99, 0, 0),
          ("French_Roast_Decaf", 49, 9.99, 0, 0)
        )

    }
  }

  def show(): Unit = {
    println("* ---")

    db withSession {
      implicit session =>
        Tables.coffees foreach { case (name, supID, price, sales, total) =>
          println("  " + name + "\t" + supID + "\t" + price + "\t" + sales + "\t" + total)
        }
    }
    println("--- *")

  }

  def removeAll(): Unit = {
    db withSession {
      implicit sesstion =>
        Tables.coffees.delete
        Tables.suppliers.delete
    }
  }


  def DSset(url: String, driver: String): Unit ={
    ds.setMinConnectionsPerPartition(2);
    ds.setMaxConnectionsPerPartition(30);
    ds.setPartitionCount(3);
    ds.setJdbcUrl("jdbc:sqlite:" + url);
    ds.setDriverClass(driver)
  }
}

