import com.shamison.sql.sqlite.SqLite
/**
 * Created by shamison on 15/03/10.
 */
object DBtest {
  def main(args: Array[String]) {
    val sqLite = new SqLite("/home/shamison/dbfiles/test.sqlite3")
//    sqLite.init()
    sqLite.post()
    sqLite.show()
    sqLite.removeAll()
    sqLite.show()
  }
}
